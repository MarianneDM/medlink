/**
 * Created by Steven Van Eenoo
 */
'use strict';

var config       = require('../config');
var gulp         = require('gulp');
var gulpif       = require('gulp-if');
var handleErrors = require('../util/handleErrors');
var browserSync  = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var concatCss    = require('gulp-concat-css');
var less         = require('gulp-less');

// less plugins
var LessPluginCleanCSS      = require('less-plugin-clean-css');
var LessPluginAutoPrefix    = require('less-plugin-autoprefix');
var cleancss    = new LessPluginCleanCSS({ advanced: true });
var autoprefix  = new LessPluginAutoPrefix({browsers: ["last 2 versions", "> 1%", "ie 9"]});

gulp.task('lessStyles', function () {
    return gulp.src(config.styles.less)
        .pipe(less({
            plugins: global.isProd ? [cleancss, autoprefix] : []
        }))
        .on('error', handleErrors)
        .pipe(gulp.dest(config.styles.dest))
        .pipe(gulpif(browserSync.active, browserSync.reload({ stream: true })));
});

gulp.task('cssStyles', function () {
    return gulp.src(config.styles.css)
        .pipe(concatCss('bundle.css', { rebaseUrls:false }))
        .pipe(autoprefixer("last 2 versions", "> 1%", "ie 9"))
        .on('error', handleErrors)
        .pipe(gulp.dest(config.styles.dest))
        .pipe(gulpif(browserSync.active, browserSync.reload({ stream: true })));
});

gulp.task('styles', function() {
    gulp.start(['lessStyles', 'cssStyles']);
});
