/**
 * Created by Steven Van Eenoo
 */
'use strict';

var gulp        = require('gulp');
var runSequence = require('run-sequence');
var argv        = require('yargs').argv;

gulp.task('default', ['clean'], function(cb) {

    cb = cb || function() {};

    global.isProd = !!argv.prod;

    runSequence(['styles', 'images', 'fonts', 'views', 'browserify'], 'watch', cb);
});
