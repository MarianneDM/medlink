/**
 * Created by Steven Van Eenoo
 */
'use strict';

var config       = require('../config');
var gulp         = require('gulp');
var gulpif       = require('gulp-if');
var source       = require('vinyl-source-stream');
var sourcemaps   = require('gulp-sourcemaps');
var buffer       = require('vinyl-buffer');
var streamify    = require('gulp-streamify');
var watchify     = require('watchify');
var browserify   = require('browserify');
var uglify       = require('gulp-uglify');
var handleErrors = require('../util/handleErrors');
var browserSync  = require('browser-sync');
var ngAnnotate   = require('browserify-ngannotate');
var bundleLogger = require('../util/bundleLogger');
var brfs         = require('brfs');
var babelify     = require('babelify');
var mold         = require('mold-source-map');

function buildScript(file) {

    var bundler = browserify({
        entries: config.browserify.entries,
        debug: true,
        cache: {},
        packageCache: {},
        fullPaths: true
    }, watchify.args);

    if ( !global.isProd ) {
        bundler = watchify(bundler);
        bundler.on('update', function() {
            rebundle();
        });
    }

    var transforms = [
        babelify.configure({
            presets: ['es2015']
        }),
        ngAnnotate,
        brfs
    ];

    transforms.forEach(function(transform) {
        bundler.transform(transform);
    });

    function rebundle() {
        var stream = bundler.bundle();
        var createSourcemap = global.isProd && config.browserify.sourcemap;
        bundleLogger.start();
        return stream.on('error', handleErrors)
            .pipe(mold.transformSourcesRelativeTo('./'))
            .pipe(source(file))
            .pipe(gulpif(createSourcemap, buffer()))
            .pipe(gulpif(createSourcemap, sourcemaps.init()))
            .pipe(gulpif(global.isProd, streamify(uglify({
                compress: { drop_console: false }
            }))))
            .pipe(gulpif(createSourcemap, sourcemaps.write('./')))
            .on('end', bundleLogger.end)
            .pipe(gulp.dest(config.scripts.dest))
            .pipe(gulpif(browserSync.active, browserSync.reload({ stream: true, once: true })));
    }
    return rebundle();
}

gulp.task('browserify', function() {
    return buildScript('main.js');
});
