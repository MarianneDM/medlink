/**
 * Created by Steven Van Eenoo
 */
'use strict';

var config        = require('../config');
var gulp          = require('gulp');

gulp.task('watch', ['browserSync'], function() {
    var stylesToWatch = config.styles.watch.concat(config.styles.css);

    gulp.watch(stylesToWatch,       ['styles']);
    gulp.watch(config.images.src,   ['images']);
    gulp.watch(config.fonts.src,    ['fonts']);
    gulp.watch(config.views.watch,  ['views']);
});
