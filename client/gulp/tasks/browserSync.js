/**
 * Created by Steven Van Eenoo
 */
'use strict';

var config      = require('../config');
var browserSync = require('browser-sync');
var gulp        = require('gulp');
var url         = require('url');
var proxy       = require('proxy-middleware');

gulp.task('browserSync', function() {

    var proxyOpts = url.parse(config.backendUrl);
    proxyOpts.route = '/Api';
    var browserSyncConfig = {
        port: config.serverport,
        server: {
            baseDir: config.dist,
            middleware: [proxy(proxyOpts)]
        },
        logPrefix: 'DPGP Medlink',
        browser: ["google chrome"],
        minify: false,
        notify: false
    };
    browserSync(browserSyncConfig);
});
