'use strict';

const config = {

  backendUrl: 'http://localhost:82/',

  serverport: 3000,

  dist: 'dist',

  views: {
    src: ['src/js/components/**/*.html','src/js/directives/**/*.html'],
    dest: 'src/js',
    watch: [
      'src/index.html',
      'src/js/components/**/*.html',
      'src/js/directives/**/*.html',
    ],
  },

  browserify: {
    entries: ['./src/js/main.js'],
    bundleName: 'main.js',
    sourcemap: true,
  },

  scripts: {
    src: 'src/js/**/*.js',
    dest: 'dist/js',
  },

  images: {
    src: 'src/assets/images/**/*',
    dest: 'dist/assets/images',
  },

  fonts: {
    src: [
      'src/assets/fonts/*',
      // vendor fonts
      'node_modules/font-awesome/fonts/*',
    ],
    dest: 'dist/assets/fonts',
  },

  styles: {
    less: 'src/assets/css/main.less',
    // vendor css files
    css: [
      './node_modules/font-awesome/css/font-awesome.css',
      './node_modules/angular-toastr/dist/angular-toastr.css',
      './node_modules/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css',
      './node_modules/ui-select/dist/select.css'
    ],
    dest: 'dist/assets/css',
    watch: ['src/assets/css/*.less', 'src/assets/css/*.css'],
  },
};

module.exports = config;
