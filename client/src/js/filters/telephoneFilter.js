'use strict';

import _ from 'lodash';

/**
 * @ngInject
 */
module.exports = function() {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, $element, $attrs, ngModelCtrl) {
            scope.$watch($attrs.ngModel, function(value) {
                if (typeof value !== "undefined" && value != "" && value != null || value == 0) {
                    let formatted = value.replace(/\D/g, '');

                    // subtract 0 if value starts with 0
                    if (_.startsWith(formatted, '0')) {
                        formatted = formatted.substr(1);
                    }

                    ngModelCtrl.$setViewValue(formatted);
                    ngModelCtrl.$render();
                }
            });
        }
    }
};
