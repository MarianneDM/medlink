'use strict';

import angular from 'angular';
import TelephoneFilter from './telephoneFilter';

export default angular
  .module('dpgpMedlink.filters', [])

    .directive('telephoneFilter', TelephoneFilter)
;
