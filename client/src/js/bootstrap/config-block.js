'use strict';

// @ngInject
function configBlock($urlRouterProvider, $translateProvider, $compileProvider,
	$stateProvider, $httpProvider, $uibTooltipProvider, calendarConfig, localStorageServiceProvider, moment) {

	if (window.location.hostname !== 'localhost') {
		$compileProvider.debugInfoEnabled(false);
	}
	$httpProvider.useApplyAsync(true);

	//Fix for CORS problem on Chrome
	$httpProvider.defaults.headers.common = {};
	$httpProvider.defaults.headers.post = {};
	$httpProvider.defaults.headers.put = {};
	$httpProvider.defaults.headers.patch = {};

	moment.locale('en_gb', {
		  week : {
		    	dow : 1 // Monday is the first day of the week
		  }
	});

	calendarConfig.allDateFormats.moment.title.day = 'dddd D MMMM';
	calendarConfig.allDateFormats.moment.date.hour = 'H';
	calendarConfig.showTimesOnWeekView = true;
	calendarConfig.displayEventEndTimes = true;
	calendarConfig.dateFormatter = "moment";
	calendarConfig.templates.calendarDayView = 'calenderDayView.html';
	calendarConfig.templates.calendarWeekView = 'calenderWeekView.html';

	localStorageServiceProvider.setPrefix('DPGP_medlink');

	// default route
  $urlRouterProvider.otherwise('/afsluitingen');

	$stateProvider
		.state('app', {
        abstract: true,
        template: '<app ng-cloak show-tabs="true"></app>'
    })
		.state('appNoTabs', {
        abstract: true,
        template: '<app ng-cloak show-tabs="false"></app>'
    })
    .state('login', {
        url: '/login',
        parent: 'appNoTabs',
        template: '<login></login>'
    })
		.state('register', {
        url: '/register',
        parent: 'appNoTabs',
        template: '<register></register>'
    })
    .state('home', {
        url: '/home',
        parent: 'app',
        template: '<home></home>'
    })
    .state('profile', {
        url: '/profile',
        parent: 'app',
        template: '<profile></profile>'
    })
    .state('afsluitingen', {
        url: '/afsluitingen',
        parent: 'app',
        template: '<afsluitingen></afsluitingen>'
    })
    .state('openstaand', {
        url: '/openstaand',
        parent: 'app',
        template: '<openstaand></openstaand>'
    })
    .state('e-invoice', {
        url: '/e-invoice',
        parent: 'app',
        template: '<e-invoice></e-invoice>'
    })
		;

	$translateProvider.translations('en', require('../../assets/locales/en'));
	$translateProvider.translations('nl', require('../../assets/locales/nl'));
	$translateProvider.preferredLanguage('nl');
	$translateProvider.useSanitizeValueStrategy('escaped');
}

module.exports = configBlock;
