'use strict';

import angular from 'angular';

import Home from './home';
import HomeCtrl from './homeCtrl';

export default (function() {
    angular.module('dpgpMedlink.components.home', [])

    .directive('home', Home)
    .controller('HomeCtrl', HomeCtrl)
    ;
    
})();
