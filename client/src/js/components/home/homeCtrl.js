'use strict';

class HomeCtrl {

  // @ngInject
  constructor(moment) {
    _.assign(this, {
        moment
    });
    this.title = 'Home';

    //These variables MUST be set as a minimum for the calendar to work
    this.calendarView = 'week';
    this.viewDate = new Date();
    this.events = [
      {
        title: 'This is a really long event title that occurs on every year',
        type: 'important',
        startsAt: moment().startOf('day').add(7, 'hours').add(15, 'minutes').toDate(),
        endsAt: moment().startOf('day').add(9, 'hours').toDate(),
        draggable: true,
        resizable: true
      }, {
        title: '222This222 is a really long event title that occurs on every year',
        type: 'success',
        startsAt: moment().startOf('day').add(10, 'hours').add(30, 'minutes').toDate(),
        endsAt: moment().startOf('day').add(17, 'hours').add(15, 'minutes').toDate(),
        draggable: true,
        resizable: true
      }
    ];

    this.isCellOpen = true;
  }

  eventClicked(event) {
    console.log('Clicked', event);
  };

  eventEdited(event) {
    console.log('Edited', event);
  };

  eventDeleted(event) {
    console.log('Deleted', event);
  };

  eventTimesChanged(event) {
    console.log('Dropped or resized', event);
  };

  toggle($event, field, event) {
    $event.preventDefault();
    $event.stopPropagation();
    event[field] = !event[field];
  };

  rangeSelected(startDate, endDate) {
      this.events.push({
        title: 'Something',
        type: 'special',
        startsAt: this.moment(startDate).toDate(),
        endsAt: this.moment(endDate).toDate(),
        draggable: true,
        resizable: true
      });
  };

}

export default HomeCtrl;
