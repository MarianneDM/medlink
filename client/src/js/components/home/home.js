'use strict';

function Home() {
    return {
        templateUrl: 'home/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
    };
}

module.exports = Home;
