'use strict';

function Login() {
    return {
        templateUrl: 'login/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
    };
}

module.exports = Login;
