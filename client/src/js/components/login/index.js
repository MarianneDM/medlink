'use strict';

import angular from 'angular';

import Login from './login';
import LoginCtrl from './loginCtrl';

export default (function() {
    angular.module('dpgpMedlink.components.login', [])

    .directive('login', Login)
    .controller('LoginCtrl', LoginCtrl)
    ;
    
})();
