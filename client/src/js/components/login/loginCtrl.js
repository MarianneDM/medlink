'use strict';

class LoginCtrl {

	// @ngInject
	constructor(UserService, $state) {
		_.assign(this, {
			UserService,
			$state
		});
	}

	login() {
		let data = {           
			Username: "EXT-MDEM",
			Password: "***"           
		};

		this.UserService.login(data).then(res => {
			console.log(res);
			this.UserService.getUserInfo().then(info => {
				console.log(info);
				this.UserService.setHeaderData(info);
				this.$state.go('afsluitingen');
			});
		}).catch(err => {
			console.log(err);
			this.error = true;
		});
	}

}

export default LoginCtrl;
