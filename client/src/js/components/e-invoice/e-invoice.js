'use strict';

function EInvoice() {
    return {
        templateUrl: 'e-invoice/e-invoice.html',
        controller: 'EInvoiceCtrl',
        controllerAs: 'vm'
    };
}

module.exports = EInvoice;
