'use strict';

import angular from 'angular';

import EInvoice from './e-invoice';
import EInvoiceCtrl from './e-invoiceCtrl';

export default (function() {
    angular.module('dpgpMedlink.components.e-invoice', [])

    .directive('eInvoice', EInvoice)
    .controller('EInvoiceCtrl', EInvoiceCtrl)
    ;

})();
