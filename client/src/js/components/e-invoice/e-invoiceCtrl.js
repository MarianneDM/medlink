'use strict';

class EInvoiceCtrl {

	// @ngInject
	constructor(DummyDataService) {
		_.assign(this, {  });

		this.invoice = DummyDataService.getInvoice();
		this.originalObj = angular.copy(this.invoice);

		this.percentages = ["0%", "6%", "18%", "21%"];
	}

	calculateBTW(amount, perc) {
		return amount * perc.replace(/\D/g, '')/100;
	}

	calculateTotal() {
		return _.sumBy(this.invoice.lines, 'bedrag');
	}

	calculateTotalPercentage() {
		let total = 0;
		this.invoice.lines.forEach(line => {
			total += line.bedrag * line.percentage.replace(/\D/g, '')/100;
		});
		return total;
	}

	cancel() {
		console.log(this.originalObj);
		this.invoice = this.originalObj;
	}

	save() {
		console.log("saving", this.originalObj);
	}

	send() {
		console.log("sending", this.originalObj);
	}

}

export default EInvoiceCtrl;
