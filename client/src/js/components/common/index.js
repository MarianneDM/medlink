'use strict';

import angular from 'angular';

import Header from './header/header';
import HeaderCtrl from './header/headerCtrl';

import Logos from './logos/logos';

export default (function() {
    angular.module('dpgpMedlink.components.common', [])

    .directive('header', Header)
    .controller('HeaderCtrl', HeaderCtrl)

    .directive('logos', Logos)
    ;

})();
