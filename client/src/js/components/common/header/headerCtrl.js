'use strict';
import _ from 'lodash';

class HeaderCtrl {

  // @ngInject
  constructor($state, $translate, $scope, UserService) {
    _.assign(this, { $state, $translate, UserService });
    this.languages = ['en', 'nl'];
    this.currentLanguage = $translate.use();
    this.showTabs = $scope.showTabs;
    this.user = this.UserService.getLoggedInUser();
  }

  switchLanguage(lang) {
    this.$translate.use(lang);
    this.currentLanguage = lang;
  }

  logout() {
    this.UserService.logout();
    this.$state.go('login', {}, {reload: true});
  }
}

export default HeaderCtrl;
