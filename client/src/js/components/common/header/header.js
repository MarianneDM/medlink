'use strict';

function Header() {
    return {
        scope: {
          showTabs: '='
        },
        templateUrl: 'common/header/header.html',
        controller: 'HeaderCtrl',
        controllerAs: 'header'
    };
}

module.exports = Header;
