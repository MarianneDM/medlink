'use strict';

import angular from 'angular';

import Register from './register';
import RegisterCtrl from './registerCtrl';

export default (function() {
    angular.module('dpgpMedlink.components.register', [])

    .directive('register', Register)
    .controller('RegisterCtrl', RegisterCtrl)
    ;

})();
