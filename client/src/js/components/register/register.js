'use strict';

function Register() {
    return {
        templateUrl: 'register/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'register'
    };
}

module.exports = Register;
