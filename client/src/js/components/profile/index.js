'use strict';

import angular from 'angular';

import Profile from './profile';
import ProfileCtrl from './profileCtrl';

export default (function() {
    angular.module('dpgpMedlink.components.profile', [])

    .directive('profile', Profile)
    .controller('ProfileCtrl', ProfileCtrl)
    ;

})();
