'use strict';

function Profile() {
    return {
        templateUrl: 'profile/profile.html',
        controller: 'ProfileCtrl',
        controllerAs: 'vm'
    };
}

module.exports = Profile;
