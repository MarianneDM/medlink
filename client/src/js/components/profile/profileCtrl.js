'use strict';

class ProfileCtrl {

  // @ngInject
  constructor(DummyDataService) {
    this.dateOptions = {
      startingDay: 1,
      showWeeks: false,
      maxDate: new Date()
    };
    this.sexes = ['Man', 'Vrouw'];
    this.statuten = ['Freelancer', 'Leverancier'];

    this.info = DummyDataService.getProfileInfo();
    this.originalInfo = angular.copy(this.info);
  }

  addAttachment(file) {
    this.info.attachments.push(file);
  }

  openAttachment(attachmentId) {
    console.log(attachmentId);
  }

  cancel() {
    this.info = this.originalInfo;
  }

  save() {
    //Make call to MDLW
    console.log("Saving", this.info);
  }

}

export default ProfileCtrl;
