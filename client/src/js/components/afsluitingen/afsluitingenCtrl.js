'use strict';

class AfsluitingenCtrl {

	// @ngInject
	constructor(DummyDataService, RESTService) {
		_.assign(this, {  });

		this.types = [
			{
				id: 1,
				name: 'Foto'
			},
			{
				id: 2,
				name: 'Artikel'
			},
			{
				id: 3,
				name: 'Onkosten'
			}
		];

		// RESTService.getPerformanceTypes().then(res => {
		// 	console.log(res);
		// 	this.types = res;
		// });

		RESTService.getEdities().then(res => {
			this.edities = res;
		});

		RESTService.getRedacties().then(res => {
			this.redacties = res;
		});

		this.messages = [
			{
				description: 'Vanaf 2015 wordt de voorheffing berekend in ons financieel systeem!'
			},
			{
				description: 'Opgelet: Onkosten op tijd indienen!'
			}
		];

		this.filters = {};

		this.afsluitingen = DummyDataService.getAfsluitingen();
	}

	closeAlert(index) {
    this.messages.splice(index, 1);
  }

	changeFilters() {
    console.log("filters changed", this.filters);
  }

	loadMore() {
		console.log("load more data");
	}

}

export default AfsluitingenCtrl;
