'use strict';

function Afsluitingen() {
    return {
        templateUrl: 'afsluitingen/afsluitingen.html',
        controller: 'AfsluitingenCtrl',
        controllerAs: 'vm'
    };
}

module.exports = Afsluitingen;
