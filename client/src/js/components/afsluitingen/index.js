'use strict';

import angular from 'angular';

import Afsluitingen from './afsluitingen';
import AfsluitingenCtrl from './afsluitingenCtrl';

export default (function() {
    angular.module('dpgpMedlink.components.afsluitingen', [])

    .directive('afsluitingen', Afsluitingen)
    .controller('AfsluitingenCtrl', AfsluitingenCtrl)
    ;

})();
