'use strict';

import angular from 'angular';

import App from './app/app';
import './common';
import './home';
import './login';
import './register';
import './profile';
import './afsluitingen';
import './openstaand';
import './e-invoice';

export default angular
  .module('dpgpMedlink.components', [
    'dpgpMedlink.components.common',
    'dpgpMedlink.components.home',
    'dpgpMedlink.components.login',
    'dpgpMedlink.components.register',
    'dpgpMedlink.components.profile',
    'dpgpMedlink.components.afsluitingen',
    'dpgpMedlink.components.openstaand',
    'dpgpMedlink.components.e-invoice',
  ])

  .directive('app', App)
;
