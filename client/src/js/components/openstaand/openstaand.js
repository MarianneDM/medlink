'use strict';

function Openstaand() {
    return {
        templateUrl: 'openstaand/openstaand.html',
        controller: 'OpenstaandCtrl',
        controllerAs: 'vm'
    };
}

module.exports = Openstaand;
