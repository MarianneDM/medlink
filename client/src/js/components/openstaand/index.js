'use strict';

import angular from 'angular';

import Openstaand from './openstaand';
import OpenstaandCtrl from './openstaandCtrl';

import Onkosten from './onkosten/onkosten';
import OnkostenCtrl from './onkosten/onkostenCtrl';

import Uren from './uren/uren';
import UrenCtrl from './uren/urenCtrl';

import Extra from './extra/extra';
import ExtraCtrl from './extra/extraCtrl';

import Wijzigingen from './wijzigingen/wijzigingen';
import WijzigingenCtrl from './wijzigingen/wijzigingenCtrl';

import Kilometers from './kilometers/kilometers';
import KilometersCtrl from './kilometers/kilometersCtrl';

export default (function() {
    angular.module('dpgpMedlink.components.openstaand', [])

    .directive('openstaand', Openstaand)
    .controller('OpenstaandCtrl', OpenstaandCtrl)

    .directive('onkosten', Onkosten)
    .controller('OnkostenCtrl', OnkostenCtrl)

    .directive('uren', Uren)
    .controller('UrenCtrl', UrenCtrl)

    .directive('extra', Extra)
    .controller('ExtraCtrl', ExtraCtrl)

    .directive('wijzigingen', Wijzigingen)
    .controller('WijzigingenCtrl', WijzigingenCtrl)

    .directive('kilometers', Kilometers)
    .controller('KilometersCtrl', KilometersCtrl)
    ;

})();
