'use strict';

function Uren() {
    return {
        templateUrl: 'openstaand/uren/uren.html',
        controller: 'UrenCtrl',
        controllerAs: 'uren'
    };
}

module.exports = Uren;
