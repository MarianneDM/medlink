'use strict';

function Extra() {
    return {
        templateUrl: 'openstaand/extra/extra.html',
        controller: 'ExtraCtrl',
        controllerAs: 'extra'
    };
}

module.exports = Extra;
