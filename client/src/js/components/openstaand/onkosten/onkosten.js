'use strict';

function Onkosten() {
    return {
        templateUrl: 'openstaand/onkosten/onkosten.html',
        controller: 'OnkostenCtrl',
        controllerAs: 'vm'
    };
}

module.exports = Onkosten;
