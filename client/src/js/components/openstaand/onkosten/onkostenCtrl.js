'use strict';

import _ from 'lodash';

class OnkostenCtrl {

  // @ngInject
  constructor(DummyDataService, DummyRESTService, OpenstaandeService) {
    _.assign(this, { OpenstaandeService });

    this.showAddSection = false;
    this.filters = {};
    this.afsluitingen = DummyDataService.getOnkosten();

    this.dateFormat = 'dd-MM-yyyy';
    this.dateOptions = {
      startingDay: 1,
      showWeeks: false,
      maxDate: new Date()
    };

    this.originalObjArr = [];

    this.types = ["Kilometervergoeding", "Horeca/hotel", "Telefonie", "Hardware"];
    this.jaartallen = [2016, 2015, 2014];
    this.statusses = DummyRESTService.getStatusses();
  }

  edit(item) {
    //Copy the original object
    let originalObj = angular.copy(item);
    this.originalObjArr.push(originalObj);

    item.edit = true;
  }

  delete(afsluitingId, uitgaveId, prestatieId, onkostenId) {
    this.OpenstaandeService.removeItem(this.afsluitingen, afsluitingId, uitgaveId, prestatieId, onkostenId);
  }

  cancel(item) {
    //Reset object to original state
    this.OpenstaandeService.revertItem(this.afsluitingen, _.find(this.originalObjArr, {'id': item.id}));

    //Remove original object from array
    _.remove(this.originalObjArr, {
      id: item.id
    });

    item.edit = false;
  }

  save(item) {
    //Remove original object from array
    _.remove(this.originalObjArr, {
      id: item.id
    });

    item.edit = false;
    console.log("saving", item);
  }

  addOnkost() {
    this.showAddSection = true;
  }

  cancelNew() {
    this.newItem = {};
    this.selectedPrestatie = null;
    this.isLosstaand = false;
    this.showAddSection = false;
  }

  saveNew() {
    console.log(this.newItem);
    this.showAddSection = false;
  }

  addAttachment(file) {
    console.log(file);

  }

  changeFilters() {
    console.log("filters changed", this.filters);
  }

	loadMore() {
		console.log("load more data");
	}

}

export default OnkostenCtrl;
