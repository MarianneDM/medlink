'use strict';

import _ from 'lodash';

class KilometersCtrl {

  // @ngInject
  constructor(DummyDataService, DummyRESTService, OpenstaandeService) {
    _.assign(this, { OpenstaandeService });

    // this.showAddSection = false;
    this.filters = {};

    this.dateFormat = 'dd-MM-yyyy';
    this.dateOptions = {
      startingDay: 1,
      showWeeks: false,
      maxDate: new Date()
    };

    // this.lines = [{}];

    this.jaartallen = [2016, 2015, 2014];
    this.statusses = DummyRESTService.getStatusses();

    this.afsluitingen = DummyDataService.getKMAfsluitingen();
  }

  // addOnkost() {
  //   this.showAddSection = true;
  // }

  // addLine() {
  //   this.lines.push({});
  // }

  // linesAreValid() {
  //   return _.every(this.lines, 'selectedPrestatie');
  // }

  // cancelNew() {
  //   this.lines = [{}];
  //   this.showAddSection = false;
  // }

  // saveNew() {
  //   console.log(this.lines);
  //   this.showAddSection = false;
  // }

  changeFilters() {
    console.log("filters changed", this.filters);
  }

	loadMore() {
		console.log("load more data");
	}

}

export default KilometersCtrl;
