'use strict';

function Kilemeters() {
    return {
        templateUrl: 'openstaand/kilometers/kilometers.html',
        controller: 'KilometersCtrl',
        controllerAs: 'vm'
    };
}

module.exports = Kilemeters;
