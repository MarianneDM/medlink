'use strict';

function Wijzigingen() {
    return {
        templateUrl: 'openstaand/wijzigingen/wijzigingen.html',
        controller: 'WijzigingenCtrl',
        controllerAs: 'wijziging'
    };
}

module.exports = Wijzigingen;
