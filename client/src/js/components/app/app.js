'use strict';

function App() {
    return {
        scope: {
          showTabs: '='
        },
        templateUrl: 'app/app.html'
    };
}

module.exports = App;
