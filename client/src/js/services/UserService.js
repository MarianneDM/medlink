'use strict';

import _ from 'lodash';


class UserService {

	// @ngInject
	constructor($http, $q, DummyUserService, localStorageService) {
		_.assign(this, {
			$http,
			$q,
			DummyUserService,
			localStorageService
		});
	}

	getUrl(sUrl) {
		const prefix = '';

		// 	const port = parseInt(window.location.port, 10);
		//   if (window.location.hostname === 'localhost' && (port === 3000 || port === 82 || port === 85)) {
		//  		return `Api/Api${prefix}${sUrl}`;
		//  	}
		return 'http://localhost:82/Api' + prefix + sUrl;
	}

	getDefaultParams() {
		return 'UserId=' + this.localStorageService.get('userid') + '&UserToken=' + this.localStorageService.get('token');
	}

	getLoggedInUser() {
		return {
      id: this.localStorageService.get("userid"),
      name: this.localStorageService.get("username")
    }
	}

	login(data) {
		if (window.location.hostname === 'localhost' && parseInt(window.location.port, 10) === 3000) {
			//Do things and return dummy data
			let resp = this.DummyUserService.login();
			this.localStorageService.set("userid", resp.UserId);
			this.localStorageService.set("token", resp.UserToken);
			return this.$q.when(resp);
		} else {
			return this.$http({
					method: 'POST',
					url: this.getUrl('/User/Login'),
					data
				}).then(res => {
					this.localStorageService.set("userid", res.data.UserId);
					this.localStorageService.set("token", res.data.UserToken);
					return res.data;
				})
				.catch(err => {
					console.log("Error with login");
					return this.$q.reject(err);
				});
		}
	}

	logout() {
		//Clear all local storage entries
		this.localStorageService.remove("userid", "username", "token");
	}

	getUserInfo() {
		if (window.location.hostname === 'localhost' && parseInt(window.location.port, 10) === 3000) {
			//Do things and return dummy data
			let resp = this.DummyUserService.getUserInfo();
			return this.$q.when(resp);
		} else {
			return this.$http.get(
				this.getUrl('/User/GetUserInfo?' + this.getDefaultParams())
			).then(res => {
				return res.data
			}).catch(() => {
				console.log("error with fetching user info");
				return this.$q.reject();
			});
		}
	}

	setHeaderData(data) {
		this.localStorageService.set("username", data.FIRSTNAME + " " + data.LASTNAME);
	}

}

export default UserService;
