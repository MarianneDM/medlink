'use strict';

class OpenstaandeService {

	removeItem(arr, afslId, uitgId, presId, onkId) {
		arr.forEach(afsl => {
			if (afsl.id === afslId) {
				afsl.uitgaves.forEach(uitg => {
					if (uitg.id === uitgId) {
						if (onkId) {
							uitg.prestaties.forEach(pres => {
								if (pres.id === presId) {
									pres.onkosten.forEach(onk => {
										_.remove(pres.onkosten, {
											id: onkId
										});
									});
								}
							});
						} else {
							_.remove(uitg.prestaties, {
								id: presId
							});
						}
					}
				});
			}
		});
	}

	revertItem(arr, item) {
		arr.forEach(afsl => {
			afsl.uitgaves.forEach(uitg => {
				uitg.prestaties.forEach(pres => {
					if (pres.onkosten) {
						pres.onkosten.forEach(onk => {
							if (onk.id === item.id) {
								_.assign(onk, item);
							}
						});
					}
				});
			});
		});
	}

}

export default OpenstaandeService;
