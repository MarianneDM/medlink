'use strict';

class RESTService {

  // @ngInject
  constructor($http, $q, DummyRESTService, localStorageService, toastr, Upload) {
    _.assign(this, {
        $http,
        $q,
        DummyRESTService,
        localStorageService,
        toastr,
        Upload
    });
  }

  getUrl(sUrl) {
    const prefix = '';
		// 	const port = parseInt(window.location.port, 10);
		//   if (window.location.hostname === 'localhost' && (port === 3000 || port === 82 || port === 85)) {
		//  		return `Api/Api${prefix}${sUrl}`;
		//  	}
		return 'http://localhost:82/Api' + prefix + sUrl;
  }

  getDefaultParams() {
		return 'UserId=' + this.localStorageService.get('userid') + '&UserToken=' + this.localStorageService.get('token');
	}

  getEdities() {
    if (window.location.hostname === 'localhost' && parseInt(window.location.port, 10) === 3000) {
			//Do things and return dummy data
			let resp = this.DummyRESTService.getEdities();
			return this.$q.when(resp);
		} else {
			return this.$http.get(
				this.getUrl('/Data/GetPublicationEditions?' + this.getDefaultParams())
			).then(res => {
				return res.data
			}).catch(() => {
				console.log("error with fetching publication editions");
				return this.$q.reject();
			});
		}
  }

  getRedacties() {
    if (window.location.hostname === 'localhost' && parseInt(window.location.port, 10) === 3000) {
			//Do things and return dummy data
			let resp = this.DummyRESTService.getRedacties();
			return this.$q.when(resp);
		} else {
			return this.$http.get(
				this.getUrl('/Data/GetPublicationRedactions?' + this.getDefaultParams())
			).then(res => {
				return res.data
			}).catch(() => {
				console.log("error with fetching publication redactions");
				return this.$q.reject();
			});
		}
  }

  getPerformanceTypes() {
    if (window.location.hostname === 'localhost' && parseInt(window.location.port, 10) === 3000) {
			//Do things and return dummy data
			let resp = this.DummyRESTService.getPerformanceTypes();
			return this.$q.when(resp);
		} else {
			return this.$http.get(
				this.getUrl('/Data/GetPerformanceTypes?' + this.getDefaultParams())
			).then(res => {
				return res.data
			}).catch(() => {
				console.log("error with fetching performance types");
				return this.$q.reject();
			});
		}
  }

  uploadAttachment(file) {
      return this.Upload.upload({
          url: this.getUrl('/api/upload'),
          file: file
      }).progress(function(evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
      }).success(function(data, status, headers, config) {
          console.log('file ' + config.file.name + ' uploaded. Response: ' + data);
          return status;
      }).error(function(err) {
          this.toastr.error('Bijlage opslaan mislukt!', err);
          return this.$q.reject(err);
      });
    }
}

export default RESTService;
