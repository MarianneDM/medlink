'use strict';

class DummyDataService {

	getAfsluitingen() {
		return [{
			name: 'Juli 2016',
			bedrag: 500.00,
			voorheffing: 100.00,
			uitgaves: [{
				name: 'AD',
				bedrag: 100.00,
				voorheffing: 25.00,
				prestaties: [{
					date: new Date(),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					editie: 'NL',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 50.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}, {
					date: new Date(),
					editie: 'BE',
					pagina: 15,
					redactie: 'REGIO',
					bedrag: 30.00,
					omschrijving: 'Vlaamse Olympische Spelen',
					thumb: '4'
				}, {
					date: new Date('2016-05-07'),
					editie: 'BE',
					pagina: 20,
					redactie: 'REGIO',
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					thumb: '1',
					onkosten: [{
						date: new Date('2016-07-21'),
						soort: {
							text: 'Horeca/hotel',
							icon: 'fa-bed'
						},
						omschrijving: 'Ibis in Rotterdam',
						bedrag: 123.00
					}, {
						date: new Date('2016-07-16'),
						soort: {
							text: 'Kilometervergoeding',
							icon: 'fa-car'
						},
						omschrijving: '234 km',
						bedrag: 50.00
					}]
				}, {
					omschrijving: 'Losse onkosten',
					bedrag: 770.00,
					onkosten: [{
						date: new Date('2016-07-16'),
						soort: {
							text: 'Telefonie',
							icon: 'fa-phone'
						},
						omschrijving: 'Maandelijkse factuur',
						bedrag: 20.00
					}, {
						date: new Date('2016-07-01'),
						soort: {
							text: 'Hardware',
							icon: 'fa-cogs'
						},
						omschrijving: 'Aankoop laptop',
						bedrag: 750.00
					}]
				}]
			}, {
				name: 'HLN',
				bedrag: 300.00,
				voorheffing: 65.00,
				prestaties: [{
					date: new Date(),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 220.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					editie: 'NL',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}, {
				name: 'DM',
				bedrag: 100.00,
				voorheffing: 10.00,
				prestaties: [{
					date: new Date(),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					editie: 'BE',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}]
		}, {
			name: 'Juni 2016',
			bedrag: 200.00,
			voorheffing: 25.00,
			uitgaves: [{
				name: 'AD',
				bedrag: 100.00,
				voorheffing: 25.00,
				prestaties: [{
					date: new Date(),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					editie: 'NL',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 50.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}, {
					date: new Date(),
					editie: 'BE',
					pagina: 15,
					redactie: 'REGIO',
					bedrag: 30.00,
					omschrijving: 'Vlaamse Olympische Spelen',
					thumb: '4'
				}, {
					date: new Date(),
					editie: 'BE',
					pagina: 20,
					redactie: 'REGIO',
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					thumb: '1',
					onkosten: [{
						date: new Date('2016-05-11'),
						soort: {
							text: 'Horeca/hotel',
							icon: 'fa-bed'
						},
						omschrijving: 'Ibis in Rotterdam',
						bedrag: 123.00
					}, {
						date: new Date('2016-05-07'),
						soort: {
							text: 'Kilometervergoeding',
							icon: 'fa-car'
						},
						omschrijving: '234 km',
						bedrag: 50.00
					}]
				}]
			}, {
				name: 'HLN',
				bedrag: 300.00,
				voorheffing: 65.00,
				prestaties: [{
					date: new Date(),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 220.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					editie: 'NL',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}, {
				name: 'DM',
				bedrag: 100.00,
				voorheffing: 10.00,
				prestaties: [{
					date: new Date(),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					editie: 'BE',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}]
		}];
	}

	getOnkosten() {
		return [{
			id: 1,
			name: 'Juni 2016',
			bedrag: 500.00,
			uitgaves: [{
				id: 11,
				name: 'AD',
				bedrag: 100.00,
				prestaties: [{
					id: 111,
					date: new Date('2016-06-21'),
					editie: 'BE',
					pagina: 20,
					redactie: 'REGIO',
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					thumb: '1',
					onkosten: [{
						id: 1111,
						date: new Date('2016-07-08'),
						soort: {
							text: 'Horeca/hotel',
							icon: 'fa-bed'
						},
						omschrijving: 'Ibis in Rotterdam',
						bedrag: 123.00,
						status: {
							text: 'PENDING',
							color: 'orange'
						}
					}, {
						id: 1112,
						date: new Date('2016-07-08'),
						soort: {
							text: 'Kilometervergoeding',
							icon: 'fa-car'
						},
						omschrijving: '234 km',
						bedrag: 50.00,
						status: {
							text: 'REJECTED',
							color: 'red',
							comments: 'Te veel kilometers'
						}
					}]
				}, {
					id: 112,
					date: new Date('2016-06-16'),
					soort: {
						text: 'Telefonie',
						icon: 'fa-phone'
					},
					editie: 'NL',
					redactie: 'SPORT',
					bedrag: 20.00,
					omschrijving: 'Maandelijkse factuur'
				}, {
					id: 113,
					date: new Date('2016-06-06'),
					soort: {
						text: 'Hardware',
						icon: 'fa-cogs'
					},
					editie: 'BE',
					redactie: 'REGIO',
					bedrag: 750.00,
					omschrijving: 'Aankoop laptop'
				}, ]
			}, {
				id: 12,
				name: 'HLN',
				bedrag: 300.00,
				prestaties: [{
					id: 121,
					date: new Date('2016-06-14'),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 220.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					id: 122,
					date: new Date('2016-06-10'),
					editie: 'NL',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}, {
				id: 13,
				name: 'DM',
				bedrag: 100.00,
				prestaties: [{
					id: 131,
					date: new Date('2016-06-09'),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					id: 132,
					date: new Date('2016-06-06'),
					editie: 'BE',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}]
		}, {
			id: 2,
			name: 'Mei 2016',
			bedrag: 200.00,
			uitgaves: [{
				name: 'AD',
				bedrag: 100.00,
				prestaties: [{
					id: 21,
					date: new Date('2016-05-28'),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					id: 22,
					date: new Date('2016-05-27'),
					editie: 'NL',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 50.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}, {
					id: 23,
					date: new Date('2016-05-26'),
					editie: 'BE',
					pagina: 15,
					redactie: 'REGIO',
					bedrag: 30.00,
					omschrijving: 'Vlaamse Olympische Spelen',
					thumb: '4'
				}, {
					id: 24,
					date: new Date('2016-05-16'),
					editie: 'BE',
					pagina: 20,
					redactie: 'REGIO',
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					thumb: '1',
					onkosten: [{
						id: 241,
						date: new Date('2016-06-11'),
						soort: {
							text: 'Horeca/hotel',
							icon: 'fa-bed'
						},
						omschrijving: 'Ibis in Rotterdam',
						bedrag: 123.00,
						status: {
							text: 'PENDING',
							color: 'orange'
						}
					}, {
						id: 242,
						date: new Date('2016-06-07'),
						soort: {
							text: 'Kilometervergoeding',
							icon: 'fa-car'
						},
						omschrijving: '234 km',
						bedrag: 50.00,
						status: {
							text: 'REJECTED',
							color: 'red'
						}
					}]
				}]
			}, {
				id: 22,
				name: 'HLN',
				bedrag: 300.00,
				prestaties: [{
					id: 221,
					date: new Date('2016-05-05'),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 220.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					id: 222,
					date: new Date('2016-05-01'),
					editie: 'NL',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}, {
				id: 23,
				name: 'DM',
				bedrag: 100.00,
				prestaties: [{
					id: 231,
					date: new Date('2016-05-13'),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					id: 232,
					date: new Date('2016-05-11'),
					editie: 'BE',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}]
		}];
	}

	getPrestaties() {
		return [
			{
				date: new Date(),
				editie: 'BE',
				pagina: 12,
				redactie: 'ALGEMEEN',
				uitgave: 'Humo',
				bedrag: 20.00,
				omschrijving: 'Nieuw voetbalplein',
				thumb: '1'
			}, {
				date: new Date(),
				editie: 'NL',
				pagina: 21,
				redactie: 'SPORT',
				uitgave: 'HLN',
				bedrag: 50.00,
				omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
				thumb: '2'
			}, {
				date: new Date(),
				editie: 'BE',
				pagina: 15,
				redactie: 'REGIO',
				uitgave: 'Nina',
				bedrag: 30.00,
				omschrijving: 'Vlaamse Olympische Spelen',
				thumb: '4'
			}, {
				date: new Date(),
				editie: 'BE',
				pagina: 20,
				redactie: 'REGIO',
				uitgave: 'HLN.BE',
				omschrijving: 'Artikel bouwwerven in Vlaanderen',
				thumb: '1'
			}
		];
	}

	getUren() {
		return [{
			id: 1,
			name: 'Juli 2016',
			bedrag: 500.00,
			uitgaves: [{
				id: 11,
				name: 'AD',
				bedrag: 100.00,
				prestaties: [{
					id: 111,
					date: new Date('2016-07-21'),
					editie: 'BE',
					redactie: 'REGIO',
					bedrag: 100,
					hours: 7.6,
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}]
			}, {
				id: 12,
				name: 'HLN',
				bedrag: 300.00,
				prestaties: [{
					id: 121,
					date: new Date('2016-07-28'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 220.00,
					hours: 7.6,
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 122,
					date: new Date('2016-07-26'),
					editie: 'NL',
					redactie: 'SPORT',
					bedrag: 80.00,
					hours: 3.8,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'REJECTED',
						color: 'red'
					}
				}]
			}, {
				id: 13,
				name: 'DM',
				bedrag: 100.00,
				prestaties: [{
					id: 131,
					date: new Date('2016-07-22'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					hours: 2,
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 132,
					date: new Date('2016-07-20'),
					editie: 'BE',
					redactie: 'SPORT',
					bedrag: 80.00,
					hours: 5,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'REJECTED',
						color: 'red'
					}
				}]
			}]
		}, {
			id: 2,
			name: 'Juni 2016',
			bedrag: 800.00,
			uitgaves: [{
				name: 'AD',
				bedrag: 250.00,
				prestaties: [{
					id: 21,
					date: new Date('2016-06-25'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 50.00,
					hours: 2,
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'REJECTED',
						color: 'red'
					}
				}, {
					id: 22,
					date: new Date('2016-06-24'),
					editie: 'NL',
					redactie: 'SPORT',
					bedrag: 150.00,
					hours: 7,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 23,
					date: new Date('2016-06-23'),
					editie: 'BE',
					redactie: 'REGIO',
					bedrag: 30,
					hours: 4,
					omschrijving: 'Vlaamse Olympische Spelen',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 24,
					date: new Date('2016-06-22'),
					editie: 'BE',
					redactie: 'REGIO',
					bedrag: 20,
					hours: 7.6,
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}]
			}, {
				id: 22,
				name: 'HLN',
				bedrag: 300.00,
				prestaties: [{
					id: 221,
					date: new Date('2016-06-19'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 220.00,
					hours: 7.6,
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 222,
					date: new Date('2016-06-18'),
					editie: 'NL',
					redactie: 'SPORT',
					bedrag: 80.00,
					hours: 7.6,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}]
			}, {
				id: 23,
				name: 'DM',
				bedrag: 250.00,
				prestaties: [{
					id: 231,
					date: new Date('2016-06-12'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 200,
					hours: 7.6,
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'REJECTED',
						color: 'red'
					}
				}, {
					id: 232,
					date: new Date('2016-06-06'),
					editie: 'BE',
					redactie: 'SPORT',
					bedrag: 50,
					hours: 7.6,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}]
			}]
		}];
	}

	getProfileInfo() {
		return {
			id: 1102455,
			aanspreking: "dhr",
			name: "De Roo",
			first_name: "Frank",
			sex: "Man",
			voorvoegsel: "",
			voorletters: "",
			email: "frank.de.roo@deroofotografie.be",
			street: "sluisweg",
			houseNo: "1",
			busNO: "",
			zip: 9000,
			city: "Gent",
			statuut: "Freelancer",
			birthday: new Date("1990-06-06"),
			snb: "",
			contactpersoon: "Marianne De Maesschalck",
			bank: {
				iban: "BE67 1234 5678 98",
				bic: "BEXXXXXX",
				bankNrIsNotEuropean: true
			},
			telIsoCode: "+32",
			telNr: "21234567",
			gsmIsoCode: "+32",
			gsmNr: "0472987654",
			isVoorwaardenChecked: true,
			isSchijnzelfstandigheidChecked: true,
			attachments: [
				{
					id: 1,
					name: 'ID.png',
					type: 'image/png'
				}, {
					id: 2,
					name: 'contract_DE_ROO.pdf',
					type: 'application/pdf'
				}
			]
		};
	}

	getExtraPrestaties() {
		return [{
			id: 1,
			name: 'Juni 2016',
			bedrag: 500.00,
			uitgaves: [{
				id: 11,
				name: 'AD',
				bedrag: 100.00,
				prestaties: [{
					id: 111,
					date: new Date('2016-06-21'),
					editie: 'BE',
					redactie: 'REGIO',
					bedrag: 100,
					type: 'Foto',
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}]
			}, {
				id: 12,
				name: 'HLN',
				bedrag: 300.00,
				prestaties: [{
					id: 121,
					date: new Date('2016-06-28'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 220.00,
					type: 'Artikel',
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 122,
					date: new Date('2016-06-26'),
					editie: 'NL',
					redactie: 'SPORT',
					bedrag: 80.00,
					type: 'Artikel',
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'REJECTED',
						color: 'red'
					}
				}]
			}, {
				id: 13,
				name: 'DM',
				bedrag: 100.00,
				prestaties: [{
					id: 131,
					date: new Date('2016-06-22'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					type: 'Artikel',
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 132,
					date: new Date('2016-06-20'),
					editie: 'BE',
					redactie: 'SPORT',
					bedrag: 80.00,
					type: 'Foto',
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'REJECTED',
						color: 'red'
					}
				}]
			}]
		}, {
			id: 2,
			name: 'Mei 2016',
			bedrag: 800.00,
			uitgaves: [{
				name: 'AD',
				bedrag: 250.00,
				prestaties: [{
					id: 21,
					date: new Date('2016-05-25'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 50.00,
					type: 'Reportage',
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'REJECTED',
						color: 'red'
					}
				}, {
					id: 22,
					date: new Date('2016-05-24'),
					editie: 'NL',
					redactie: 'SPORT',
					bedrag: 150.00,
					type: 'Artikel',
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 23,
					date: new Date('2016-05-23'),
					editie: 'BE',
					redactie: 'REGIO',
					bedrag: 30,
					type: 'Artikel',
					omschrijving: 'Vlaamse Olympische Spelen',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 24,
					date: new Date('2016-05-22'),
					editie: 'BE',
					redactie: 'REGIO',
					bedrag: 20,
					type: 'Column',
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}]
			}, {
				id: 22,
				name: 'HLN',
				bedrag: 300.00,
				prestaties: [{
					id: 221,
					date: new Date('2016-05-19'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 220.00,
					type: 'Artikel',
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}, {
					id: 222,
					date: new Date('2016-05-18'),
					editie: 'NL',
					redactie: 'SPORT',
					bedrag: 80.00,
					type: 'Artikel',
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}]
			}, {
				id: 23,
				name: 'DM',
				bedrag: 250.00,
				prestaties: [{
					id: 231,
					date: new Date('2016-05-12'),
					editie: 'BE',
					redactie: 'ALGEMEEN',
					bedrag: 200,
					type: 'Artikel',
					omschrijving: 'Nieuw voetbalplein',
					status: {
						text: 'REJECTED',
						color: 'red'
					}
				}, {
					id: 232,
					date: new Date('2016-05-06'),
					editie: 'BE',
					redactie: 'SPORT',
					bedrag: 50,
					type: 'Artikel',
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					status: {
						text: 'PENDING',
						color: 'orange'
					}
				}]
			}]
		}];
	}

	getWijzigingen() {
		return [{
			id: 1,
			name: 'Juni 2016',
			bedrag: 182.00,
			uitgaves: [{
				id: 11,
				name: 'AD',
				bedrag: 182.00,
				prestaties: [{
					id: 111,
					date: new Date('2016-06-21'),
					editie: 'BE',
					pagina: 20,
					redactie: 'REGIO',
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					thumb: '1',
					wijzigingen: [{
						id: 1111,
						date: new Date('2016-07-08'),
						soort: {
							text: 'Horeca/hotel',
							icon: 'fa-bed'
						},
						omschrijving: 'Ibis in Rotterdam',
						bedrag: 123.00,
						status: {
							text: 'PENDING',
							color: 'orange'
						}
					}, {
						id: 1112,
						date: new Date('2016-07-08'),
						soort: {
							text: 'Kilometervergoeding',
							icon: 'fa-car'
						},
						omschrijving: '234 km',
						bedrag: 50.00,
						status: {
							text: 'REJECTED',
							color: 'red',
							comments: 'Te veel kilometers'
						}
					}]
				}, {
					id: 112,
					date: new Date('2016-06-16'),
					soort: {
						text: 'Telefonie',
						icon: 'fa-phone'
					},
					editie: 'NL',
					redactie: 'SPORT',
					bedrag: 5.00,
					omschrijving: 'Maandelijkse factuur'
				}, {
					id: 113,
					date: new Date('2016-06-06'),
					soort: {
						text: 'Hardware',
						icon: 'fa-cogs'
					},
					editie: 'BE',
					redactie: 'REGIO',
					bedrag: 4.00,
					omschrijving: 'Aankoop laptop'
				}, ]
			}]
		}, {
			id: 2,
			name: 'Mei 2016',
			bedrag: 200.00,
			uitgaves: [{
				name: 'AD',
				bedrag: 100.00,
				prestaties: [{
					id: 21,
					date: new Date('2016-05-28'),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					id: 22,
					date: new Date('2016-05-27'),
					editie: 'NL',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 50.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}, {
					id: 23,
					date: new Date('2016-05-26'),
					editie: 'BE',
					pagina: 15,
					redactie: 'REGIO',
					bedrag: 30.00,
					omschrijving: 'Vlaamse Olympische Spelen',
					thumb: '4'
				}, {
					id: 24,
					date: new Date('2016-05-16'),
					editie: 'BE',
					pagina: 20,
					redactie: 'REGIO',
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					thumb: '1',
					wijzigingen: [{
						id: 241,
						date: new Date('2016-06-11'),
						soort: {
							text: 'Horeca/hotel',
							icon: 'fa-bed'
						},
						omschrijving: 'Ibis in Rotterdam',
						bedrag: 123.00,
						status: {
							text: 'PENDING',
							color: 'orange'
						}
					}, {
						id: 242,
						date: new Date('2016-06-07'),
						soort: {
							text: 'Kilometervergoeding',
							icon: 'fa-car'
						},
						omschrijving: '234 km',
						bedrag: 50.00,
						status: {
							text: 'PENDING',
							color: 'orange'
						}
					}]
				}]
			}, {
				id: 22,
				name: 'HLN',
				bedrag: 300.00,
				prestaties: [{
					id: 221,
					date: new Date('2016-05-05'),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 220.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					id: 222,
					date: new Date('2016-05-01'),
					editie: 'NL',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}, {
				id: 23,
				name: 'DM',
				bedrag: 100.00,
				prestaties: [{
					id: 231,
					date: new Date('2016-05-13'),
					editie: 'BE',
					pagina: 12,
					redactie: 'ALGEMEEN',
					bedrag: 20.00,
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					id: 232,
					date: new Date('2016-05-11'),
					editie: 'BE',
					pagina: 21,
					redactie: 'SPORT',
					bedrag: 80.00,
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}]
		}];
	}

	getInvoice() {
		return {
			factuurnr: 16282,
			lines: [{
				date: new Date('2016-07-23'),
				editie: 'BE',
				redactie: 'ALGEMEEN',
				bedrag: 22.38,
				omschrijving: 'Nieuw voetbalplein',
				type: 'Correctie',
				percentage: "21%"
			}, {
				date: new Date('2016-07-21'),
				editie: 'NL',
				redactie: 'SPORT',
				bedrag: 50.00,
				omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
				type: 'Artikel',
				clausule: 'Some testing text',
				percentage: "0%"
			}, {
				date: new Date('2016-07-15'),
				editie: 'BE',
				redactie: 'REGIO',
				bedrag: 30.00,
				omschrijving: 'Vlaamse Olympische Spelen',
				type: 'Foto',
				percentage: "0%"
			}, {
				date: new Date('2016-07-10'),
				editie: 'BE',
				redactie: 'REGIO',
				bedrag: 210,
				omschrijving: 'Ibis in Rotterdam',
				type: 'Onkosten',
				percentage: "6%"
			}]
		};
	}

	getKMAfsluitingen() {
		return [{
			name: 'Juli 2016',
			uitgaves: [{
				name: 'AD',
				prestaties: [{
					date: new Date(),
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}, {
					date: new Date(),
					omschrijving: 'Vlaamse Olympische Spelen',
					thumb: '4'
				}, {
					date: new Date('2016-05-07'),
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					thumb: '1'
				}]
			}, {
				name: 'HLN',
				prestaties: [{
					date: new Date(),
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1',
					km: {
						kms: 20,
						amount: 10
					}
				}, {
					date: new Date(),
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}, {
				name: 'DM',
				prestaties: [{
					date: new Date(),
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}]
		}, {
			name: 'Juni 2016',
			uitgaves: [{
				name: 'AD',
				prestaties: [{
					date: new Date(),
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2',
					km: {
						kms: 18,
						amount: 9
					}
				}, {
					date: new Date(),
					omschrijving: 'Vlaamse Olympische Spelen',
					thumb: '4',
					km: {
						kms: 23,
						amount: 11.5
					}
				}, {
					date: new Date(),
					omschrijving: 'Artikel bouwwerven in Vlaanderen',
					thumb: '1'
				}]
			}, {
				name: 'HLN',
				prestaties: [{
					date: new Date(),
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}, {
				name: 'DM',
				prestaties: [{
					date: new Date(),
					omschrijving: 'Nieuw voetbalplein',
					thumb: '1'
				}, {
					date: new Date(),
					omschrijving: 'Rode Duivels winnen EK 2016 in Frankrijk',
					thumb: '2'
				}]
			}]
		}];
	}

}

export default DummyDataService;
