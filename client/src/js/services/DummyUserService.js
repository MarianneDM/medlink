'use strict';

class DummyUserService {

	login() {
		return {
			"UserId": "130001",
			"UserToken": "75A21D835D9737D77E23B1AA0D2B57BF"
		};
	}

	getUserInfo() {
		return {
			"ID": "130001",
			"SEARCHNAME": "MarianneeDMDM consul",
			"SALUTATION": "",
			"COMPANYNAME": "DM consultings",
			"LASTNAME": "Dummy",
			"FIRSTNAME": "Mariannee",
			"NOMINALLETTERS": "",
			"LANGUAGE": "nl",
			"EMAIL": "marianne.demaesschalck@persgroep.be",
			"MEDLINKLOGIN": "EXT-MDEM",
			"MEDLINKCOMMUNICATIONSENT": "0",
			"BLOCKFORPAYMENT": "0",
			"RMBNUMBER": "",
			"NOTES": "WHOEHOEWWWW\n",
			"PAYPICTURE": "0",
			"CURRENCYCODE": "EUR",
			"RECVERSION": "1747900223",
			"RECID": "5637144829"
		};
	}

}

export default DummyUserService;
