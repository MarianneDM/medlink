'use strict';
import angular from 'angular';
import RESTService from './RESTService';
import DummyDataService from './DummyDataService';
import OpenstaandeService from './OpenstaandeService';
import UserService from './UserService';
import DummyUserService from './DummyUserService';
import DummyRESTService from './DummyRESTService';

export default angular
  .module('dpgpMedlink.services', [])

  .service('RESTService', RESTService)
  .service('DummyDataService', DummyDataService)
  .service('OpenstaandeService', OpenstaandeService)
  .service('UserService', UserService)
  .service('DummyUserService', DummyUserService)
  .service('DummyRESTService', DummyRESTService)

;
