'use strict';

import angular from 'angular';

// angular modules
import ngUiRouter from 'angular-ui-router';
import ngTranslate from 'angular-translate';
import ngToastr from 'angular-toastr';
import 'angular-sanitize';
import 'angular-ui-bootstrap';
import 'moment';
import 'angular-bootstrap-calendar';
import 'ui-select';
import 'ng-file-upload';
import 'angular-local-storage';

// application modules
import './templates';
import components from './components';
import services from './services';
import directives from './directives';
import filters from './filters';

// bootstrap
import configBlock from './bootstrap/config-block';
import runBlock from './bootstrap/run-block.js';

angular.element(document).ready(() => {
  const modulesToLoad = [
    components.name,
    services.name,
    directives.name,
    filters.name,
    ngUiRouter,
    ngTranslate,
    ngToastr,
    'templates',
    'ngSanitize',
    'ui.bootstrap',
    'mwl.calendar',
    'ui.select',
    'ngFileUpload',
    'LocalStorageModule'
  ];

  angular
    .module('dpgpMedlink', modulesToLoad)
    .config(configBlock)
    .run(runBlock);

  angular.bootstrap(document, ['dpgpMedlink'], {
    strictDi: true,
  });

  if (window.location.hostname === 'localhost') {
    const showAngularStats = require('ng-stats');
    showAngularStats({
      position: 'bottomright',
    });
  }
});
