'use strict';

/**
* https://gist.github.com/mlynch/dd407b93ed288d499778
*/

// @ngInject
function Autofocus($timeout) {
    return {
        restrict: 'A',
        link: function ($scope, $element) {
            $timeout(function () {
                $element[0].focus();
            });
        }
    };
}

module.exports = Autofocus;
