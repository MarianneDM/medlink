'use strict';

// @ngInject
module.exports = function enterAction() {
  return {
    restrict: 'A',
    link: (scope, element, attrs) => {
      element.bind('keydown keypress', (event) => {
        if (event.which === 13) {
          scope.$apply(() => {
            scope.$eval(attrs.enterAction);
          });
          event.preventDefault();
        }
      });
    },
  };
};
