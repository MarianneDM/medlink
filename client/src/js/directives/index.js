'use strict';

import angular from 'angular';
import enterAction from './enter-action';
import arrow from './arrow/arrow';
import status from './status/status';
import helpIcon from './helpIcon/helpIcon';
import dropdownPrestaties from './dropdownPrestaties/dropdownPrestaties';
import DropdownPrestatiesCtrl from './dropdownPrestaties/dropdownPrestatiesCtrl';
import autofocus from './autofocus/autofocus';
import customInput from './customInput/customInput';

export default angular
  .module('dpgpMedlink.directives', [])

  .directive('enterAction', enterAction)
  .directive('arrow', arrow)
  .directive('status', status)
  .directive('helpIcon', helpIcon)
  .directive('dropdownPrestaties', dropdownPrestaties)
  .controller('DropdownPrestatiesCtrl', DropdownPrestatiesCtrl)
  .directive('autofocus', autofocus)
  .directive('customInput', customInput)
;
