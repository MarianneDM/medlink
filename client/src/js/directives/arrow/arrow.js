'use strict';

function Arrow() {
    return {
        scope: {
          icon: '=',
          isSub: '='
        },
        templateUrl: 'arrow/arrow.html'
    };
}

module.exports = Arrow;
