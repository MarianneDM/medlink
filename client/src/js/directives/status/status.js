'use strict';

function Status() {
    return {
        scope: {
          color: '=',
          text: '='
        },
        templateUrl: 'status/status.html'
    };
}

module.exports = Status;
