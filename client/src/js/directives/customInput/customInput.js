'use strict';

function CustomInput() {
	return {
		scope: {
			customName: '@',
			customType: '@',
			translationKey: '@',
			model: '=',
			isRequired: '='
		},
		replace: true,
		templateUrl: 'customInput/customInput.html'
	};
}

module.exports = CustomInput;
