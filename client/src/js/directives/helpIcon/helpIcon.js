'use strict';

function HelpIcon() {
    return {
        scope: {
          text: '='
        },
        templateUrl: 'helpIcon/helpIcon.html'
    };
}

module.exports = HelpIcon;
