'use strict';

import _ from 'lodash';

class DropdownPrestatiesCtrl {

  // @ngInject
  constructor(DummyDataService, $timeout) {
    _.assign(this, { $timeout });

    this.showInput = false;
    this.prestaties = DummyDataService.getPrestaties();
  }

  changeInputStatus() {
    this.showInput = !this.showInput;
  }

  selectPrestatie(prestatie) {
    this.selected = prestatie;
    this.showInput = false;
    this.isLos = false;
  }

  changeIsLosStatus(event) {
    //To enable ng-click in ng-click
    event.stopPropagation();
    this.isLos = !this.isLos;
  }

  onBlur() {
    let that = this;
    this.$timeout(function() { //Because ngBlur is executed before ngClick
       that.showInput = false;
    }, 100);
  }

}

export default DropdownPrestatiesCtrl;
