'use strict';

function DropdownPrestaties() {
    return {
        bindToController: {
          selected: '=',
          isLos: '=?',
          losstaande: '=?',
          showText: '=?'
        },
        replace: true,
        templateUrl: 'dropdownPrestaties/dropdownPrestaties.html',
        controller: 'DropdownPrestatiesCtrl',
        controllerAs: 'dropdown'
    };
}

module.exports = DropdownPrestaties;
