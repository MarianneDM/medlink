#Persgroep - Medlink

Web application for customer De Persgroep for their Freelancers.


## Getting up and running

1. Run `npm install` from the root directory
2. Run `npm start` in a command prompt window
3. Your browser will automatically be opened and directed to `localhost:3000`
4. For deployment to production, run the `npm run build` task (Note: This won't provide you with browser-sync's live reloading as watchify is not enabled)

All files in the `/dist` folder will be served to the server. Any changes in the `/src` folder will automatically be processed by Gulp.
